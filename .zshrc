# common-shell
eval "$(cat $HOME/.config/common-shell/modules/global/*)"
eval "$(cat $HOME/.config/common-shell/modules/posix/*)"

# starship prompt
eval "$(starship init zsh)"
