function edit_commit
    if not [ (count $argv) -eq 1 ]
        echo "usage: edit_commit <commit sha>"\n
        echo "change 'pick' to 'edit' for the commit, commit changes, and continue rebase"
        return
    end
    git rebase -i "$argv[1]^"
end

function gclone
    if not [ (count $argv) -eq 1 ]
        echo "usage: gclone <git repository>"\n
        echo "clone a git repository into an organized subfolder, similar to go mod"
        return
    end
    set -l path (echo "$argv[1]" | sed 's/.*:\/\///' | sed 's/:/\//g' | sed 's/\.git$//' | sed 's/^git@//')
    set -l path "$CODEPATH/$path"
    mkdir -p "$path"
    git clone --bare "$argv[1]" "$path/.bare"
    zoxide add "$path"
    # taken from https://morgan.cugerone.com/blog/workarounds-to-git-worktree-using-bare-repository-and-cannot-fetch-remote-branches/
    echo "gitdir: ./.bare" > $path/.git
    git config -f $path/.bare/config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
    git -C $path fetch origin
    set -l base_branch (git -C $path remote show origin | sed -n '/HEAD branch/s/.*: //p')
    git -C $path worktree add "$base_branch"
    cd "$path/$base_branch"
    echo "🎄 worktree was created at $path/$base_branch"
end

function gupdate
    argparse 'h/help' -- $argv
    if set -q _flag_help
        echo "usage: gupdate [options] [<filter>]"
        echo "options:"
        echo "  -h, --help      Show help"
        return 0
    end
    set all_dirs $CODEPATH/*/*/*
    if [ (count $argv) -eq 1 ]
        set all_dirs (find "$CODEPATH/$argv[1]" -maxdepth 3 -type d -name ".bare" -exec dirname {} \;)
    end
    parallel  'git -C {} fetch && base_branch=$(cd {} && git remote show origin | sed -n \'/HEAD branch/s/.*: //p\') git -C {}/$base_branch pull origin $base_branch' ::: $all_dirs
end
