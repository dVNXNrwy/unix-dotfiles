{ config, pkgs, lib, unstable, ... }:
let 
  shaders_dir = "${pkgs.mpv-shim-default-shaders}/share/mpv-shim-default-shaders/shaders";
in {
  home.packages = [
    pkgs._1password
    pkgs.alacritty
    pkgs.buf-language-server
    pkgs.cargo
    unstable.charm-freeze
    pkgs.clang-tools
    pkgs.confetty
    pkgs.coreutils
    pkgs.erlang
    pkgs.ffmpeg
    pkgs.ffmpegthumbnailer
    pkgs.glab
    pkgs.gleam
    unstable.go
    unstable.gopls
    unstable.golangci-lint
    unstable.golangci-lint-langserver
    pkgs.gnupg
    pkgs.gum
    pkgs.imagemagick
    unstable.jdt-language-server
    pkgs.jsonnet-language-server
    pkgs.lua
    pkgs.lua-language-server
    pkgs.marksman
    pkgs.mediainfo
    unstable.mpv-shim-default-shaders
    unstable.monaspace
    pkgs.nil
    pkgs.nixd
    pkgs.nodejs
    pkgs.nodePackages.bash-language-server
    pkgs.nodePackages.typescript-language-server
    pkgs.nodePackages_latest.prettier
    pkgs.pyright
    pkgs.python3
    pkgs.python311Packages.adblock
    pkgs.python311Packages.python-lsp-server
    pkgs.ranger
    pkgs.regols
    pkgs.rsync
    pkgs.rustc
    pkgs.rust-analyzer
    pkgs.shellcheck
    pkgs.shfmt
    pkgs.slides
    pkgs.starlark-rust
    pkgs.terraform-ls
    pkgs.texlab
    pkgs.tflint
    pkgs.tokei
    pkgs.typioca
    pkgs.tzdata
    pkgs.unar
    pkgs.unzip
    unstable.vhs
    pkgs.vscode-langservers-extracted
    pkgs.wezterm
    pkgs.yaml-language-server
    pkgs.yt-dlp
  ];

  programs = {
    gh.enable = true;

    gh.settings = {
      editor = "hx";
      git_protocol = "ssh";
    };

    mpv = {
      enable = true;
      scripts = [
        unstable.mpvScripts.modernx-zydezu
        unstable.mpvScripts.sponsorblock-minimal
        unstable.mpvScripts.thumbfast
      ];
    };
  };
  home.file = {
    ".config/mpv/shaders/" = {
      source = "${shaders_dir}/";
    };
  };

  # https://github.com/nix-community/home-manager/issues/1341
  disabledModules = [ "targets/darwin/linkapps.nix" ];
  home.activation = lib.mkIf pkgs.stdenv.isDarwin {
    copyApplications = let
      apps = pkgs.buildEnv {
        name = "home-manager-applications";
        paths = config.home.packages;
        pathsToLink = "/Applications";
      };
    in lib.hm.dag.entryAfter [ "writeBoundary" ] ''
      baseDir="$HOME/Applications/Home Manager Apps"
      if [ -d "$baseDir" ]; then
        rm -rf "$baseDir"
      fi
      mkdir -p "$baseDir"
      for appFile in ${apps}/Applications/*; do
        target="$baseDir/$(basename "$appFile")"
        $DRY_RUN_CMD cp ''${VERBOSE_ARG:+-v} -fHRL "$appFile" "$baseDir"
        $DRY_RUN_CMD chmod ''${VERBOSE_ARG:+-v} -R +w "$target"
      done
    '';
  };
}
