# keymap

Tools for using the Colemak keyboard layout

## Remap backspace on MacOS

By default, the MacOS Colemak layout does not remap the backspace key.
The provided plist can solve this.

### Installation

```shell
sudo cp com.user.backspace.plist /Library/LaunchAgents/
```

## uncolemak.go

Takes input that was typed via QWERTY when the actual layout uses Colemak.
Useful for accepting Yubikey input without having to change keyboard layouts or use special configuration.
