#!/usr/bin/env bash

# Brew
export PATH="$PATH:/usr/local/bin"
export PATH="$PATH:/opt/homebrew/bin"

# Set editor
export EDITOR="hx"
export VISUAL=$EDITOR

# Pager
export MANPAGER=ov
export PAGER=ov

# Go
export GOPATH="$HOME/.go"
export PATH="$PATH:$GOPATH/bin"
# https://drewdevault.com/2021/08/06/goproxy-breaks-go.html
export GOPROXY=direct

# Rust
export PATH="$PATH:$HOME/.cargo/bin"

# ripgrep
export RIPGREP_CONFIG_PATH="$HOME/.config/ripgreprc"

# source code (organized go mod style)
export CODEPATH="$HOME/src"

# path to fish shell (for macos where SHELL is kept as zsh)
export FISHPATH=$(which fish)
