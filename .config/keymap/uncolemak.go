package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

// colemakToQwerty accepts an input string typed on Colemak and
// returns the string as if were typed on Qwerty
func colemakToQwerty(s string) string {
	colemak := "qwfpgjluy;arstdhneiozxcvbkmQWFPGJLUY:ARSTDHNEIOZXCVBKM"
	qwerty := "qwertyuiopasdfghjkl;zxcvbnmQWERTYUIOPASDFGHJKL:ZXCVBNM"
	var out string
	for _, c := range s {
		if colemakIdx := strings.IndexRune(colemak, c); colemakIdx != -1 {
			out += string(qwerty[colemakIdx])
		} else {
			out += string(c)
		}
	}
	return out
}

func main() {
	var s string
	a := os.Args[1:]
	if len(a) > 0 {
		s = strings.Join(a, " ")
	} else {
		reader := bufio.NewReader(os.Stdin)
		stdin, _ := reader.ReadString('\n')
		s = strings.TrimSpace(stdin)
	}
	fmt.Println(colemakToQwerty(s))
}
