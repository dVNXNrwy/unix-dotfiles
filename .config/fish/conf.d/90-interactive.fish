if status is-interactive
    # Commands to run in interactive sessions can go here
    # remove greeting
    set fish_greeting

    # prompt at bottom of window
    tput cup (tput lines) 0
    alias clear="clear && tput cup (tput lines) 0"

    # starship prompt
    starship init fish | source

    # starship transient prompt
    function starship_transient_prompt_func
        starship module character
    end
    starship init fish | source
    enable_transience

    fish_personal_vi_key_bindings

    # zoxide
    zoxide init --cmd cd fish | source

    export ZELLIJ_AUTO_EXIT="true"
end
