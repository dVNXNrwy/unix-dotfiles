#!/usr/bin/env bash
# dotfiles git
alias dgit='git --git-dir=$HOME/.local/share/unix-dotfiles/ --work-tree=$HOME'
# utilities
alias wanip='curl ifconfig.me'
alias pingg='ping -c 3 1.1.1.1'
alias sizesort='du -m --max-depth 1 | sort -n'
alias sizeof='du --max-depth=0 -h'
alias weather='curl wttr.in'
alias uncolemak='go run $HOME/.config/keymap/uncolemak.go'
alias ytmp3='yt-dlp --extract-audio --audio-format mp3 --audio-quality 0'
# docker
alias dstopall='docker stop (docker ps -q)'
alias linux='docker run --rm -it --pull always --network host --workdir /workdir \
    -v $PWD/:/workdir -v $HOME:/$USER \
    execweave/development'
# default tool overrides
alias cat='bat --paging=never'
alias curl='curlie'
alias dig='doggo'
alias gw='git worktree'
alias ls='eza --icons --group-directories-first'
alias unrar='unar'
# shortening
alias g='rg'
alias k='kubectl'
