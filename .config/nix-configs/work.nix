{}:

{
  system.defaults.NSGlobalDomain.AppleInterfaceStyle = "Light";
  # Automatically toggle light/dark mode
  system.defaults.NSGlobalDomain.AppleInterfaceStyleSwitchesAutomatically = true;
}
