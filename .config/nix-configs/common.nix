{
  services = {
    nix-daemon.enable = true;
  };
  programs = {
    bash.enable = true;
    fish.enable = true;
    zsh.enable = true;
  };
}
