{
  homebrew = {
    casks = [
      "monero-wallet"
      "mullvadvpn"
      "tor-browser"
      "tuta-mail"
      "veracrypt"
    ];
  };
}

