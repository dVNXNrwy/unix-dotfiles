function fish_personal_vi_key_bindings
    fish_vi_key_bindings

    # Normal
    bind -M default o forward-char
    bind -M default n backward-char
    bind -M default e down-line
    bind -M default i up-line
    bind -M default h 'set fish_bind_mode insert; commandline -f repaint'

    # Insert
    bind -s --preset -m insert l insert-line-under repaint-mode
    bind -s --preset -m insert L insert-line-over repaint-mode
    bind -M insert --erase \ci
    
    # Visual
    bind -M visual o forward-char
    bind -M visual n backward-char
    bind -M visual e down-line
    bind -M visual i up-line
end
