local wezterm = require 'wezterm'
local config = wezterm.config_builder()

-- This is where you actually apply your config choices
config.default_prog = { '/etc/profiles/per-user/mtreadwell/bin/fish', '-l' }
if (os.getenv("TERMINFO_DIRS")) then
  config.set_environment_variables = {
    TERMINFO_DIRS = os.getenv("TERMINFO_DIRS"),
    WSLENV = 'TERMINFO_DIRS',
  }
  config.term = 'wezterm'
end
config.check_for_updates = false
config.scrollback_lines = 100000
config.front_end = "WebGpu"
config.max_fps = 144

config.font = wezterm.font {
    family = 'Monaspace Neon',
    harfbuzz_features = {'ss01', 'ss02', 'ss03', 'ss04', 'ss05', 'ss06', 'ss07', 'ss08', 'calt', 'dlig'},
}
config.font_size = 12.0

config.window_decorations = "RESIZE"
config.hide_tab_bar_if_only_one_tab = true
config.window_close_confirmation = "NeverPrompt"

-- default hyperlinks rules, but exclude emails
config.hyperlink_rules = {
  -- Matches: a URL in parens: (URL)
  {
    regex = '\\((\\w+://\\S+)\\)',
    format = '$1',
    highlight = 1,
  },
  -- Matches: a URL in brackets: [URL]
  {
    regex = '\\[(\\w+://\\S+)\\]',
    format = '$1',
    highlight = 1,
  },
  -- Matches: a URL in curly braces: {URL}
  {
    regex = '\\{(\\w+://\\S+)\\}',
    format = '$1',
    highlight = 1,
  },
  -- Matches: a URL in angle brackets: <URL>
  {
    regex = '<(\\w+://\\S+)>',
    format = '$1',
    highlight = 1,
  },
  -- Then handle URLs not wrapped in brackets
  {
    regex = '\\b\\w+://\\S+[)/a-zA-Z0-9-]+',
    format = '$0',
  },
}

local act = wezterm.action
config.keys = {
  {
    key = 'f',
    mods = 'CMD',
    action = act.Search { CaseInSensitiveString = '' },
  },
  {
    key = 'w',
    mods = 'CMD',
    action = wezterm.action.CloseCurrentPane { confirm = false },
  },
  {
    key = 'v',
    mods = 'SHIFT|CTRL|ALT',
    action = wezterm.action.SplitVertical {domain="CurrentPaneDomain"},
  },
  {
    key = 'b',
    mods = 'SHIFT|CTRL|ALT',
    action = wezterm.action.SplitHorizontal {domain="CurrentPaneDomain"},
  },
  {
    key = 'n',
    mods = 'SHIFT|CTRL',
    action = wezterm.action.ActivatePaneDirection "Left",
  },
  {
    key = 'e',
    mods = 'SHIFT|CTRL',
    action = wezterm.action.ActivatePaneDirection "Down",
  },
  {
    key = 'i',
    mods = 'SHIFT|CTRL',
    action = wezterm.action.ActivatePaneDirection "Up",
  },
  {
    key = 'o',
    mods = 'SHIFT|CTRL',
    action = wezterm.action.ActivatePaneDirection "Right",
  },
  {
    key = 'r',
    mods = 'SHIFT|CTRL',
    action = wezterm.action.RotatePanes"Clockwise",
  },
}

-- wezterm.gui is not available to the mux server, so take care to
-- do something reasonable when this config is evaluated by the mux
local function get_appearance()
  if wezterm.gui then
    return wezterm.gui.get_appearance()
  end
  return 'Dark'
end

local function scheme_for_appearance(appearance)
  if appearance:find 'Dark' then
    return 'iceberg-dark'
  else
    return 'iceberg-light'
  end
end
config.color_scheme = scheme_for_appearance(get_appearance())

return config
