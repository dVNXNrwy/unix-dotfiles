{
  description = "Home Manager configuration of honor-stance-horsemen";

  inputs = {
    darwin = {
      url = "github:lnl7/nix-darwin";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils.url = "github:numtide/flake-utils";
    home-manager = {
      url = "github:nix-community/home-manager/release-23.11";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.11";
    nixpkgs-unstable.url = "github:nixos/nixpkgs/nixos-unstable";
  };

  outputs = { nixpkgs, nixpkgs-unstable, darwin, home-manager, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
          config.allowUnfree = true;
        };
        unstable = import nixpkgs-unstable {
          inherit system;
          config.allowUnfree = true;
        };
      in
      {
        
      packages.darwinConfigurations = let
        common_modules = [
          ./common.nix
          ./common-darwin.nix
          home-manager.darwinModules.home-manager
          ./homebrew-common.nix
        ];
      in {
        VXFU1FQV = darwin.lib.darwinSystem {
          inherit system;
          modules = common_modules ++ [
            ./homebrew-personal.nix
            {
              users.users.mtreadwell = {
                name = "mtreadwell";
                home = "/Users/mtreadwell";
              };
              home-manager.users.mtreadwell = {
                imports = [ 
                  ./home-common.nix
                  ./home-client.nix
                  ./home-personal.nix
                ];
              };
            }
          ];
          specialArgs = { inherit pkgs unstable; };
        };
        work = darwin.lib.darwinSystem {
          inherit system;
          modules = common_modules ++ [
            ./work.nix
            {
              users.users.mtreadwell = {
                name = "mtreadwell";
                home = "/Users/mtreadwell";
              };
              home-manager.users.mtreadwell = {
                imports = [ 
                  ./home-common.nix
                  ./home-client.nix
                ] ++ nixpkgs.lib.optional (builtins.pathExists ./home-company.nix) ./home-company.nix;
              };
            }
          ];
          specialArgs = { inherit pkgs unstable; };
        };
      };

      packages.homeConfigurations = {
        # Mainly for non-NixOS or non-Darwin systems
        server = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;
          # Specify your home configuration modules here, for example,
          # the path to your home.nix.
          modules = [
            ./home-common.nix
            ./home-linux.nix
          ];

          # Optionally use extraSpecialArgs
          # to pass through arguments to home.nix
          extraSpecialArgs = { inherit pkgs unstable; };
        };

        development = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;
          modules = [
            ./home-common.nix
            ./home-linux.nix
            ./home-client.nix
          ];
          extraSpecialArgs = { inherit pkgs unstable; };
        };
       
      };
    }
  );
}
