{ pkgs, lib, unstable, ... }:

{
  home.username = lib.mkDefault "mtreadwell";
  home.homeDirectory = lib.mkDefault "/Users/mtreadwell";

  home.stateVersion = "22.11";

  home.sessionVariables = {
    EDITOR = "hx";
    TERMINFO_DIRS = "/etc/profiles/per-user/$USER/share/terminfo";
    PATH = "$HOME/.nix-profile/bin:/etc/profiles/per-user/$USER/bin:$PATH";
  };

  home.packages = [
    pkgs.bat
    pkgs.binutils
    pkgs.btop
    pkgs.coreutils
    pkgs.curl
    pkgs.curlie
    pkgs.difftastic
    pkgs.doggo
    pkgs.du-dust
    pkgs.duf
    pkgs.easyrsa
    unstable.eza
    pkgs.fd
    pkgs.figlet
    pkgs.fish
    pkgs.fishPlugins.bass
    pkgs.fzf
    pkgs.git
    pkgs.htop
    pkgs.httpie
    pkgs.jq
    pkgs.kak-lsp
    pkgs.kakoune
    pkgs.less
    pkgs.lolcat
    unstable.majima
    pkgs.openssl
    pkgs.ov
    pkgs.parallel
    pkgs.ranger
    pkgs.ripgrep
    pkgs.rsync
    pkgs.sd
    pkgs.starship
    pkgs.tldr
    pkgs.tmux
    pkgs.tzdata
    pkgs.unar
    pkgs.unzip
    (pkgs.uutils-coreutils.override { prefix = ""; })
    pkgs.watch
    pkgs.wget
    unstable.yazi
    pkgs.yq
    pkgs.zoxide
  ];

  nix = {
    package = lib.mkDefault pkgs.nixFlakes;
    extraOptions = ''
      experimental-features = nix-command flakes
    '';
  };

  programs = {
    fish.enable = true;
    home-manager.enable = true;
    helix = {
      enable = true;
      package = unstable.helix;
    };
  };
}
