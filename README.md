# Archived

This project has moved to https://git.sr.ht/~cucumber-zoom/dotfiles

# unix-dotfiles

 [![pipeline status](https://gitlab.com/dVNXNrwy/unix-dotfiles/badges/main/pipeline.svg)](https://gitlab.com/dVNXNrwy/unix-dotfiles/-/commits/main) [![Docker Image Size (latest by date)](https://img.shields.io/docker/image-size/execweave/development)](https://hub.docker.com/r/execweave/development) [![Built with Nix](https://img.shields.io/static/v1?logo=nixos&logoColor=white&label=&message=Built%20with%20Nix&color=41439a)](https://builtwithnix.org/)

Configuration geared towards systems administration and development.

## Install

Initialize a bare repository with all files reset.
Use the `dgit` alias for interacting with the repo.

### HTTPS

```shell
git clone --bare https://gitlab.com/dVNXNrwy/unix-dotfiles.git $HOME/.local/share/unix-dotfiles && \
    alias dgit='git --git-dir=$HOME/.local/share/unix-dotfiles/ --work-tree=$HOME' && \
    dgit status
```

### SSH

```shell
git clone --bare git@gitlab.com:dVNXNrwy/unix-dotfiles.git $HOME/.local/share/unix-dotfiles && \
    alias dgit='git --git-dir=$HOME/.local/share/unix-dotfiles/ --work-tree=$HOME' && \
    dgit status
```

⚠️ *If any of the staged files shown in git status are marked as modified instead of deleted, this
represents a conflict between a file in the repo and an existing local file. These should be backed
up prior to proceeding further.*

The missing files can now be reset and restored into the home directory.

```shell
dgit reset && dgit restore .
```

## Uninstall

Run the following command to delete all git-tracked files and the underlying git store.

```shell
dgit rm -r . && rm -rf $HOME/.local/share/unix-dotfiles
```

## Nix

All required packages and system config can be installed using Nix flakes with home-manager or nix-darwin depending on the specific system.
It's still necessary to clone and install the dotfiles when using Nix.

```shell
# nix-darwin (local)
nix run nix-darwin -- switch --flake ~/.config/nix-configs/#VXFU1FQV
# nix-darwin (remote)
nix run nix-darwin -- switch --flake 'gitlab:dVNXNrwy/unix-dotfiles/?dir=.config/nix-configs/#VXFU1FQV'

# home-manager (local)
nix run home-manager/master -- switch --flake .config/nix-configs/#development
# home-manager (remote)
nix run home-manager/master -- switch --flake 'gitlab:dVNXNrwy/unix-dotfiles/?dir=.config/nix-configs/#development'
```

Supported systems: `["x86_64-linux" "aarch64-linux" "x86_64-darwin" "aarch64-darwin"]`

## Docker

A linux development container with these dotfiles and required tools is also provided.

```shell
docker run -it --rm execweave/development
```

## Applications

These configs are intended for the following software and applications

| Category | Application  |
|----------|--------------|
|OS        | macOS, Linux |
|Terminal  | WezTerm      |
|Editor    | Helix        |
|Shell     | fish         |
|WM        | yabai        |
