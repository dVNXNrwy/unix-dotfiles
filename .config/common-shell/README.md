# common-shell

Share common shell configurations between multiple shells

## Installation

### POSIX shells

Add the following snippet to the rc file for the appropriate shell(s).

```shell
# common-shell
eval "$(cat $HOME/.config/common-shell/modules/global/*)"
eval "$(cat $HOME/.config/common-shell/modules/posix/*)"
```

### Fish

Add the following snippet to `config.fish`.

```shell
# common-shell
cat $HOME/.config/common-shell/modules/global/* | source
```
