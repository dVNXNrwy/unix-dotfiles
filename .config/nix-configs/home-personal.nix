{ pkgs, unstable, ... }:

{
  home.packages = [
    pkgs.cloudflared
    pkgs.element-desktop
    pkgs.iperf
    pkgs.macfuse-stubs
    pkgs.mat2
    unstable.qbittorrent
  ];
}
